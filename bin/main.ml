[@@@warning "-32"]

open Core
module Ast = Nuro.Ast

let one_ =
  let open Ast.Term.Syntax in
  (* Π(A : Set). A -> A *)
  forall (fun a -> a @=> a)
;;

let unit_ =
  let open Ast.Term.Syntax in
  (* fun (A : Set) (x : A) -> x *)
  fun_ star (fun a -> fun_ a (fun x -> x))
;;

let bool_ = Ast.Term.Syntax.(one_ + one_)
let true_ = Ast.Term.Syntax.(inl unit_ ~type_:one_)
let false_ = Ast.Term.Syntax.(inr unit_ ~type_:one_)

let if_ cond ~then_ ~else_ ~type_ =
  let open Ast.Term.Syntax in
  match_ cond ~with_:(case (fun _ -> then_), case (fun _ -> else_)) ~type_
;;

let eq a x x' =
  let open Ast.Term.Syntax in
  (* Π(Ctx : A -> Set). Ctx x -> Ctx x' *)
  (a @=> star) @-> fun ctx -> app ctx [ x ] @=> app ctx [ x' ]
;;

let refl a x =
  let open Ast.Term.Syntax in
  (* fun (Ctx : A -> Set) (z : Ctx x) -> z *)
  fun_ (a @=> star) (fun ctx -> fun_ (app ctx [ x ]) (fun z -> z))
;;

let nat =
  let open Ast.Term.Syntax in
  (* Π(A : Set). A -> (A -> A) -> A *)
  star @-> fun a -> a @=> (a @=> a) @=> a
;;

let zero =
  let open Ast.Term.Syntax in
  (* fun (A : Set) (z : A) (s : A -> A) -> z *)
  fun_ star (fun a -> fun_ a (fun z -> fun_ (a @=> a) (fun _s -> z)))
;;

let succ n =
  let open Ast.Term.Syntax in
  (* fun (A : Set) (z : A) (s : A -> A) -> s (n [A] z s) *)
  fun_ star (fun a ->
      fun_ a (fun z -> fun_ (a @=> a) (fun s -> app s [ app n [ a; z; s ] ])))
;;

let print_term term = term |> Ast.Term.sexp_of_t |> Sexp.to_string_hum |> print_endline

let check ~label ~term ~type_  =
  assert Nuro.(beta_equal (type_check term) type_);
  (* assert (if true then false else false); *)
  print_endline [%string "Successfully checked test: %{label}"]
;;

let () =
  check ~label:"one_" ~term:one_ ~type_:(Kind Star);
  check ~label:"unit_" ~term:unit_ ~type_:one_;
  check ~label:"bool_" ~term:bool_ ~type_:(Kind Star);
  check ~label:"true_" ~term:true_ ~type_:bool_;
  check ~label:"false_" ~term:false_ ~type_:bool_;
  check ~label:"nat" ~term:nat ~type_:(Kind Star);
  check ~label:"zero" ~term:zero ~type_:nat;
  check ~label:"one" ~term:(succ zero) ~type_:nat;
  check ~label:"eq" ~term:(eq one_ unit_ unit_) ~type_:(Kind Star);
  check ~label:"refl" ~term:(refl one_ unit_) ~type_:(eq one_ unit_ unit_)
;;
