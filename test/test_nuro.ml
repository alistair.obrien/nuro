open! Core
module Ast = Nuro.Ast

let has_type ~term ~type_ = Nuro.(beta_equal (type_check term) type_)
let assert_has_type ~term ~type_ : unit = assert (has_type ~term ~type_)

let%test_unit "* : ▮" =
  let open Ast.Term.Syntax in
  assert_has_type ~term:star ~type_:box
;;

let%test_unit "fun (A B : *) -> A x B : * -> * -> *" =
  let open Ast.Term.Syntax in
  assert_has_type
    ~term:(fun_ star (fun a -> fun_ star (fun b -> a * b)))
    ~type_:(star @=> star @=> star)
;;

let%test_unit "fun (A B : *) (x : A) (y : B) -> pair x y : Π (A B : *). A -> B -> A * B" =
  let open Ast.Term.Syntax in
  assert_has_type
    ~term:
      (fun_ star (fun a ->
         fun_ star (fun b -> fun_ a (fun x -> fun_ b (fun y -> pair x y)))))
    ~type_:(forall (fun a -> forall (fun b -> a @=> b @=> (a * b))))
;;

let%test_unit "fun (A B : *) (p : A * B) -> fst p : Π (A B : *). A * B -> A" =
  let open Ast.Term.Syntax in
  assert_has_type
    ~term:(fun_ star (fun a -> fun_ star (fun b -> fun_ (a * b) (fun p -> fst p))))
    ~type_:(forall (fun a -> forall (fun b -> (a * b) @=> a)))
;;

let%test_unit "fun (A B : *) (p : A * B) -> snd p : Π (A B : *). A * B -> B" =
  let open Ast.Term.Syntax in
  assert_has_type
    ~term:(fun_ star (fun a -> fun_ star (fun b -> fun_ (a * b) (fun p -> snd p))))
    ~type_:(forall (fun a -> forall (fun b -> (a * b) @=> b)))
;;

let%test_unit "fun (A B : *) -> A + B : * -> * -> *" =
  let open Ast.Term.Syntax in
  assert_has_type
    ~term:(fun_ star (fun a -> fun_ star (fun b -> a + b)))
    ~type_:(star @=> star @=> star)
;;

let%test_unit "fun (A B : *) (x : A) -> inl x [B] : Π (A B : *). A -> A + B" =
  let open Ast.Term.Syntax in
  assert_has_type
    ~term:(fun_ star (fun a -> fun_ star (fun b -> fun_ a (fun x -> inl x ~type_:b))))
    ~type_:(forall (fun a -> forall (fun b -> a @=> (a + b))))
;;

let%test_unit "fun (A B : *) (y : B) -> inr y [A] : Π (A B : *). B -> A + B" =
  let open Ast.Term.Syntax in
  assert_has_type
    ~term:(fun_ star (fun a -> fun_ star (fun b -> fun_ b (fun y -> inr y ~type_:a))))
    ~type_:(forall (fun a -> forall (fun b -> b @=> (a + b))))
;;

let unit_ =
  let open Ast.Term.Syntax in
  (* Π(A : Set). A -> A *)
  forall (fun a -> a @=> a)
;;

let ( <> ) =
  let open Ast.Term.Syntax in
  (* fun (A : Set) (x : A) -> x *)
  fun_ star (fun a -> fun_ a (fun x -> x))
;;

let%test_unit "unit : *" =
  let open Ast.Term.Syntax in
  assert_has_type ~term:unit_ ~type_:star
;;

let%test_unit "() : unit" = assert_has_type ~term:( <> ) ~type_:unit_

let bool_ = Ast.Term.Syntax.(unit_ + unit_)
let true_ = Ast.Term.Syntax.(inl ( <> ) ~type_:unit_)
let false_ = Ast.Term.Syntax.(inr ( <> ) ~type_:unit_)

let if_ cond ~then_ ~else_ ~type_ =
  let open Ast.Term.Syntax in
  match_ cond ~with_:(case (fun _ -> then_), case (fun _ -> else_)) ~type_
;;

let%test_unit "bool : *" =
  let open Ast.Term.Syntax in
  assert_has_type ~term:bool_ ~type_:star
;;

let%test_unit "false : bool" = assert_has_type ~term:false_ ~type_:bool_
let%test_unit "true : bool" = assert_has_type ~term:true_ ~type_:bool_

let%test_unit "if_ : Π (A : *). bool -> A -> A -> A" =
  let open Ast.Term.Syntax in
  assert_has_type
    ~term:
      (fun_ star (fun a ->
         fun_ bool_ (fun cond ->
           fun_ a (fun then_ -> fun_ a (fun else_ -> if_ cond ~then_ ~else_ ~type_:a)))))
    ~type_:(forall (fun a -> bool_ @=> a @=> a @=> a))
;;

let eq a x x' =
  let open Ast.Term.Syntax in
  (* Π(Ctx : A -> Set). Ctx x -> Ctx x' *)
  (a @=> star) @-> fun ctx -> app ctx [ x ] @=> app ctx [ x' ]
;;

let%test_unit "fun (A : *) (x x' : A) -> eq A x x' : *" =
  let open Ast.Term.Syntax in
  assert_has_type
    ~term:(fun_ star (fun a -> fun_ a (fun x -> fun_ a (fun x' -> eq a x x'))))
    ~type_:(star @-> fun a -> a @=> a @=> star)
;;

let refl a x =
  let open Ast.Term.Syntax in
  (* fun (Ctx : A -> Set) (z : Ctx x) -> z *)
  fun_ (a @=> star) (fun ctx -> fun_ (app ctx [ x ]) (fun z -> z))
;;

let%test_unit "refl : Π (A : *) (x : A). eq A x x" =
  let open Ast.Term.Syntax in
  assert_has_type
    ~term:(fun_ star (fun a -> fun_ a (fun x -> refl a x)))
    ~type_:(star @-> fun a -> a @-> fun x -> eq a x x)
;;

let sym a x _y eq_xy =
  let open Ast.Term.Syntax in
  app eq_xy [ fun_ a (fun z -> eq a z x); refl a x ]
;;

let%test_unit "sym : Π (A : *) (x y : A). Eq A x y -> Eq A y x" =
  let open Ast.Term.Syntax in
  assert_has_type
    ~term:
      (fun_ star (fun a ->
         fun_ a (fun x ->
           fun_ a (fun y -> fun_ (eq a x y) (fun eq_xy -> sym a x y eq_xy)))))
    ~type_:(star @-> fun a -> a @-> fun x -> a @-> fun y -> eq a x y @=> eq a y x)
;;

let trans a x _y _z eq_xy eq_yz =
  let open Ast.Term.Syntax in
  app eq_yz [ fun_ a (fun w -> eq a x w); eq_xy ]
;;

let%test_unit "trans : Π (A : *) (x y z : A) -> Eq A x y -> Eq A y z -> Eq A x z" =
  let open Ast.Term.Syntax in
  assert_has_type
    ~term:
      (fun_ star (fun a ->
         fun_ a (fun x ->
           fun_ a (fun y ->
             fun_ a (fun z ->
               fun_ (eq a x y) (fun eq_xy ->
                 fun_ (eq a y z) (fun eq_yz -> trans a x y z eq_xy eq_yz)))))))
    ~type_:
      (star
      @-> fun a ->
      a @-> fun x -> a @-> fun y -> a @-> fun z -> eq a x y @=> eq a y z @=> eq a x z)
;;

let cong a b f x _y eq_xy =
  let open Ast.Term.Syntax in
  app eq_xy [ fun_ a (fun w -> eq b (f x) (f w)); refl b (f x) ]
;;

let%test_unit "cong : Π (A B : *)  (f : A -> B) (x y : A) -> Eq A x y -> Eq B (f x) (f y)"
  =
  let open Ast.Term.Syntax in
  assert_has_type
    ~term:
      (fun_ star (fun a ->
         fun_ star (fun b ->
           fun_ (a @=> b) (fun f ->
             fun_ a (fun x ->
               fun_ a (fun y ->
                 fun_ (eq a x y) (fun eq_xy -> cong a b (fun x -> app f [ x ]) x y eq_xy)))))))
    ~type_:
      (star
      @-> fun a ->
      star
      @-> fun b ->
      (a @=> b)
      @-> fun f ->
      a @-> fun x -> a @-> fun y -> eq a x y @=> eq b (app f [ x ]) (app f [ y ]))
;;

let%test_unit "fun (A B : *) (v : A) (f : A -> B) -> let x : A = v in f x : Π (A B : *). \
               A -> (A -> B) -> B"
  =
  let open Ast.Term.Syntax in
  assert_has_type
    ~term:
      (fun_ star (fun a ->
         fun_ star (fun b ->
           fun_ a (fun v ->
             fun_ (a @=> b) (fun f -> let_ (v, a) ~in_:(fun x -> app f [ x ]))))))
    ~type_:(forall (fun a -> forall (fun b -> a @=> (a @=> b) @=> b)))
;;

let nat =
  let open Ast.Term.Syntax in
  (* Π(A : Set). A -> (A -> A) -> A *)
  star @-> fun a -> a @=> (a @=> a) @=> a
;;

let%test_unit "nat : *" =
  let open Ast.Term.Syntax in
  assert_has_type ~term:nat ~type_:star
;;

let zero =
  let open Ast.Term.Syntax in
  (* fun (A : Set) (z : A) (s : A -> A) -> z *)
  fun_ star (fun a -> fun_ a (fun z -> fun_ (a @=> a) (fun _s -> z)))
;;

let%test_unit "zero : nat" = assert_has_type ~term:zero ~type_:nat

let succ n =
  let open Ast.Term.Syntax in
  (* fun (A : Set) (z : A) (s : A -> A) -> s (n [A] z s) *)
  fun_ star (fun a ->
    fun_ a (fun z -> fun_ (a @=> a) (fun s -> app s [ app n [ a; z; s ] ])))
;;

let rec nat_of_int n =
  if n < 0
  then raise (Invalid_argument [%string "nat_of_int: %{n#Int} < 0!"])
  else (
    match n with
    | 0 -> zero
    | n -> succ (nat_of_int (n - 1)))
;;

let%test_unit "succ : nat -> nat" =
  let open Ast.Term.Syntax in
  assert_has_type ~term:(fun_ nat (fun n -> succ n)) ~type_:(nat @=> nat)
;;

let nat_rec n ~zero ~succ ~type_ =
  let open Ast.Term.Syntax in
  app n [ type_; zero; succ ]
;;

let%test_unit "nat_rec : Π (A : *). nat -> A -> (A -> A) -> A" =
  let open Ast.Term.Syntax in
  assert_has_type
    ~term:
      (fun_ star (fun a ->
         fun_ nat (fun n ->
           fun_ a (fun zero ->
             fun_ (a @=> a) (fun succ -> nat_rec n ~zero ~succ ~type_:a)))))
    ~type_:(forall (fun a -> nat @=> a @=> (a @=> a) @=> a))
;;

let nat_add m n =
  let open Ast.Term.Syntax in
  nat_rec m ~zero:n ~succ:(fun_ nat (fun x -> succ x)) ~type_:nat
;;

let%test_unit "nat_add : nat -> nat -> nat" =
  let open Ast.Term.Syntax in
  assert_has_type
    ~term:(fun_ nat (fun m -> fun_ nat (fun n -> nat_add m n)))
    ~type_:(nat @=> nat @=> nat)
;;

let nat_mul m n =
  let open Ast.Term.Syntax in
  nat_rec m ~zero ~succ:(fun_ nat (fun x -> nat_add n x)) ~type_:nat
;;

let%test_unit "nat_mul : nat -> nat -> nat" =
  let open Ast.Term.Syntax in
  assert_has_type
    ~term:(fun_ nat (fun m -> fun_ nat (fun n -> nat_mul m n)))
    ~type_:(nat @=> nat @=> nat)
;;

let%test_unit "refl : eq nat ([m] + [n]) [m + n]" =
  for i = 0 to 50 do
    for j = 0 to 50 do
      let i_plus_j = nat_of_int (i + j) in
      assert_has_type
        ~term:(refl nat i_plus_j)
        ~type_:(eq nat (nat_add (nat_of_int i) (nat_of_int j)) i_plus_j)
    done
  done
;;

let%test_unit "refl : eq nat ([m] * [n]) [m * n]" =
  for i = 0 to 20 do
    for j = 0 to 20 do
      let i_times_j = nat_of_int (i * j) in
      assert_has_type
        ~term:(refl nat i_times_j)
        ~type_:(eq nat (nat_mul (nat_of_int i) (nat_of_int j)) i_times_j)
    done
  done
;;

let assert_eq term1 term2 : unit = assert (Nuro.beta_equal term1 term2)

let%test_unit "fun (A : *) (x : A) -> (fun (x : A) -> x) x = fun (A : *) (x : A) -> x" =
  let open Ast.Term.Syntax in
  assert_eq
    (fun_ star (fun a -> fun_ a (fun x -> app (fun_ a (fun x -> x)) [ x ])))
    (fun_ star (fun a -> fun_ a (fun x -> x)))
;;

let%test_unit "fst (1, 2) = 1" =
  let open Ast.Term.Syntax in
  assert_eq (fst (pair (nat_of_int 1) (nat_of_int 2))) (nat_of_int 1)
;;

let%test_unit "snd (1, 2) = 2" =
  let open Ast.Term.Syntax in
  assert_eq (snd (pair (nat_of_int 1) (nat_of_int 2))) (nat_of_int 2)
;;

let%test_unit "match (inl 1 [nat]) returns [nat] with (inl x -> x | inr x -> x) = 1" =
  let open Ast.Term.Syntax in
  assert_eq
    (match_
       (inl (nat_of_int 1) ~type_:nat)
       ~with_:(case (fun x -> x), case (fun x -> x))
       ~type_:nat)
    (nat_of_int 1)
;;

let%test_unit "match (inr 2 [nat]) returns [nat] with (inl x -> x | inr x -> x) = 2" =
  let open Ast.Term.Syntax in
  assert_eq
    (match_
       (inr (nat_of_int 2) ~type_:nat)
       ~with_:(case (fun x -> x), case (fun x -> x))
       ~type_:nat)
    (nat_of_int 2)
;;

let%test_unit "let x : nat = 1 in x + 2 = 3" =
  let open Ast.Term.Syntax in
  assert_eq
    (let_ (nat_of_int 1, nat) ~in_:(fun x -> nat_add x (nat_of_int 2)))
    (nat_of_int 3)
;;

let%test_unit "term =_alpha term" =
  Quickcheck.test Ast.Term.quickcheck_generator ~trials:10 ~f:(fun term ->
    assert (Nuro.alpha_equal term term))
;;

let%test_unit "term1 =_alpha term2 => term2 =_alpha term1" =
  Quickcheck.(
    test
      Generator.(tuple2 Ast.Term.quickcheck_generator Ast.Term.quickcheck_generator)
      ~trials:10
      ~f:(fun (term1, term2) ->
        if Nuro.alpha_equal term1 term2 then assert (Nuro.alpha_equal term2 term1)))
;;

let%test_unit "term1 =_alpha term2 && term2 =_alpha term3 => term1 =_alpha term3" =
  Quickcheck.(
    test
      Generator.(
        tuple3
          Ast.Term.quickcheck_generator
          Ast.Term.quickcheck_generator
          Ast.Term.quickcheck_generator)
      ~trials:10
      ~f:(fun (term1, term2, term3) ->
        if Nuro.alpha_equal term1 term2 && Nuro.alpha_equal term2 term3
        then assert (Nuro.alpha_equal term1 term3)))
;;

let%test_unit "term =_beta term" =
  Quickcheck.test ~trials:10 Ast.Term.quickcheck_generator ~f:(fun term ->
    assert (Nuro.beta_equal term term))
;;

let%test_unit "term1 =_beta term2 => term2 =_beta term1" =
  Quickcheck.(
    test
      Generator.(tuple2 Ast.Term.quickcheck_generator Ast.Term.quickcheck_generator)
      ~trials:10
      ~f:(fun (term1, term2) ->
        if Nuro.beta_equal term1 term2 then assert (Nuro.beta_equal term2 term1)))
;;

let%test_unit "term1 =_beta term2 && term2 =_beta term3 => term1 =_beta term3" =
  Quickcheck.(
    test
      Generator.(
        tuple3
          Ast.Term.quickcheck_generator
          Ast.Term.quickcheck_generator
          Ast.Term.quickcheck_generator)
      ~trials:10
      ~f:(fun (term1, term2, term3) ->
        if Nuro.beta_equal term1 term2 && Nuro.beta_equal term2 term3
        then assert (Nuro.beta_equal term1 term3)))
;;