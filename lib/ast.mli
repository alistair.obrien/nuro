open Core

module Var : sig
  type t = private int [@@deriving sexp, compare, quickcheck]

  include Comparable.S with type t := t

  val create : unit -> t
end

module Kind : sig
  type t =
    | Box
    | Star
  [@@deriving sexp, compare, quickcheck]

  include Comparable.S with type t := t

  val box : t
  val star : t
end

module Term : sig
  type t =
    | Var of Var.t
    | App of
        { fun_ : t
        ; arg : t
        }
    | Fun of
        { binder : binder
        ; body : t
        }
    | Pi of
        { binder : binder
        ; type_ : t
        }
    | Let of
        { binder : binder
        ; val_ : t
        ; in_ : t
        }
    | Prod of
        { left : t
        ; right : t
        }
    | Pair of
        { fst : t
        ; snd : t
        }
    | Fst of t
    | Snd of t
    | Sum of
        { left : t
        ; right : t
        }
    | Inl of
        { val_ : t
        ; type_ : t
        }
    | Inr of
        { val_ : t
        ; type_ : t
        }
    | Match of
        { matchee : t
        ; inl : case
        ; inr : case
        ; type_ : t
        }
    | Kind of Kind.t
  [@@deriving sexp, quickcheck]

  and binder = Var.t * t
  and case = Var.t * t

  val free_vars : t -> Var.Set.t
  val subst : t -> var:Var.t -> term:t -> t
  val subst_var : t -> var:Var.t -> var':Var.t -> t

  module Syntax : sig
    val app : t -> t list -> t
    val fun_ : t -> (t -> t) -> t
    val ( @: ) : t -> t -> t * t
    val ( @-> ) : t -> (t -> t) -> t
    val ( @=> ) : t -> t -> t
    val forall : (t -> t) -> t
    val let_ : t * t -> in_:(t -> t) -> t
    val ( * ) : t -> t -> t
    val pair : t -> t -> t
    val fst : t -> t
    val snd : t -> t
    val ( + ) : t -> t -> t
    val inl : t -> type_:t -> t
    val inr : t -> type_:t -> t
    val match_ : t -> with_:case * case -> type_:t -> t
    val case : (t -> t) -> case
    val kind : Kind.t -> t
    val star : t
    val box : t
  end
end
