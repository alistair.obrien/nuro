open! Core
open! Ast

module Ctx = struct
  type t = Term.t Var.Map.t [@@deriving sexp]

  let empty = Var.Map.empty
  let extend t ~var ~term = Var.Map.set t ~key:var ~data:term
  let lookup t var = Var.Map.find t var
end

let allowed_kinds arg_kind ret_kind =
  List.mem
    Kind.[ Star, Star; Star, Box; Box, Star; Box, Box ]
    (arg_kind, ret_kind)
    ~equal:(fun kinds1 kinds2 -> [%compare: Kind.t * Kind.t] kinds1 kinds2 = 0)
;;

let rec type_check t ~ctx =
  let open Term in
  match t with
  | Var var ->
    (match Ctx.lookup ctx var with
    | Some term -> term
    | None ->
      raise_s
        [%message "Variable is unbound in typing context" (ctx : Ctx.t) (var : Var.t)])
  | App { fun_; arg } ->
    let fun_type = type_check ~ctx fun_ |> Eval.eval in
    (match fun_type with
    | Pi { binder = var, arg_type; type_ = ret_type } ->
      let arg_type' = type_check ~ctx arg in
      if not (Eval.beta_equal arg_type arg_type')
      then raise_s [%message "Mismatching argument types" (arg_type : t) (arg_type' : t)];
      subst ~var ~term:arg ret_type
    | _ -> raise_s [%message "Application to a non-function type" (fun_ : t) (arg : t)])
  | Fun { binder = var, arg_type; body } ->
    let (_ : t) = type_check ~ctx arg_type in
    let ret_type = type_check ~ctx:(Ctx.extend ctx ~var ~term:arg_type) body in
    let fun_type = Pi { binder = var, arg_type; type_ = ret_type } in
    let (_ : t) = type_check ~ctx fun_type in
    fun_type
  | Pi { binder = var, kind; type_ } ->
    let arg_kind = type_check ~ctx kind |> Eval.eval in
    let ret_kind = type_check ~ctx:(Ctx.extend ctx ~var ~term:kind) type_ |> Eval.eval in
    (match arg_kind, ret_kind with
    | Kind arg_kind, Kind ret_kind ->
      if not (allowed_kinds arg_kind ret_kind)
      then
        raise_s
          [%message "Invalid kinds for pi-type" (arg_kind : Kind.t) (ret_kind : Kind.t)]
    | _ -> raise_s [%message "Expected kinds in pi-type" (arg_kind : t) (ret_kind : t)]);
    ret_kind
  | Prod { left; right } ->
    let left_kind = type_check ~ctx left in
    let right_kind = type_check ~ctx right in
    (match left_kind, right_kind with
    | Kind left_kind, Kind right_kind ->
      if not (allowed_kinds left_kind right_kind)
      then
        raise_s
          [%message
            "Invalid kinds for prod-type" (left_kind : Kind.t) (right_kind : Kind.t)]
    | _ ->
      raise_s [%message "Expected kinds for prod-type" (left_kind : t) (right_kind : t)]);
    right_kind
  | Sum { left; right } ->
    let left_kind = type_check ~ctx left in
    let right_kind = type_check ~ctx right in
    (match left_kind, right_kind with
    | Kind left_kind, Kind right_kind ->
      if not (allowed_kinds left_kind right_kind)
      then
        raise_s
          [%message
            "Invalid kinds for sum-type" (left_kind : Kind.t) (right_kind : Kind.t)]
    | _ ->
      raise_s [%message "Expected kinds for sum-type" (left_kind : t) (right_kind : t)]);
    right_kind
  | Pair { fst; snd } ->
    let fst_type = type_check ~ctx fst in
    let snd_type = type_check ~ctx snd in
    let prod_type = Prod { left = fst_type; right = snd_type } in
    let (_ : t) = type_check ~ctx prod_type in
    prod_type
  | Fst t ->
    let prod_type = type_check ~ctx t |> Eval.eval in
    (match prod_type with
    | Prod { left; _ } -> left
    | _ -> raise_s [%message "Fst applied to non-product type" (t : t)])
  | Snd t ->
    let prod_type = type_check ~ctx t |> Eval.eval in
    (match prod_type with
    | Prod { right; _ } -> right
    | _ -> raise_s [%message "Snd applied to non-product type" (t : t)])
  | Inl { val_; type_ = right_type } ->
    let left_type = type_check ~ctx val_ in
    let sum_type = Sum { left = left_type; right = right_type } in
    let (_ : t) = type_check ~ctx sum_type in
    sum_type
  | Inr { val_; type_ = left_type } ->
    let right_type = type_check ~ctx val_ in
    let sum_type = Sum { left = left_type; right = right_type } in
    let (_ : t) = type_check ~ctx sum_type in
    sum_type
  | Match { matchee; inl = varl, bodyl; inr = varr, bodyr; type_ } ->
    let sum_type = type_check ~ctx matchee |> Eval.eval in
    (match sum_type with
    | Sum { left; right } ->
      let inl_type = type_check ~ctx:(Ctx.extend ctx ~var:varl ~term:left) bodyl in
      let inr_type = type_check ~ctx:(Ctx.extend ctx ~var:varr ~term:right) bodyr in
      if not (Eval.beta_equal inl_type type_ && Eval.beta_equal inr_type type_)
      then raise_s [%message "Mismatching case return types"];
      type_
    | _ -> raise_s [%message "Match applied to non-sum type" (t : t)])
  | Let { binder; val_; in_ } ->
    type_check ~ctx (App { fun_ = Fun { binder; body = in_ }; arg = val_ })
  | Kind Star -> Kind Box
  | Kind Box -> raise_s [%message "No type for Box!"]
;;

let type_check t = type_check ~ctx:Ctx.empty t