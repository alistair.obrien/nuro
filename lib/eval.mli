open! Ast

val alpha_equal : Term.t -> Term.t -> bool
val eval : Term.t -> Term.t
val beta_equal : Term.t -> Term.t -> bool