open! Core
open! Ast

let rec alpha_equal t1 t2 =
  let open Term in
  match t1, t2 with
  | Var var1, Var var2 -> Var.equal var1 var2
  | Inl { val_ = t11; type_ = t12 }, Inl { val_ = t21; type_ = t22 }
  | Inr { val_ = t11; type_ = t12 }, Inr { val_ = t21; type_ = t22 }
  | Pair { fst = t11; snd = t12 }, Pair { fst = t21; snd = t22 }
  | Sum { left = t11; right = t12 }, Sum { left = t21; right = t22 }
  | Prod { left = t11; right = t12 }, Prod { left = t21; right = t22 }
  | App { fun_ = t11; arg = t12 }, App { fun_ = t21; arg = t22 } ->
    alpha_equal t11 t21 && alpha_equal t12 t22
  | ( Fun { binder = var1, annot1; body = body1 }
    , Fun { binder = var2, annot2; body = body2 } )
  | ( Pi { binder = var1, annot1; type_ = body1 }
    , Pi { binder = var2, annot2; type_ = body2 } ) ->
    alpha_equal annot1 annot2 && alpha_equal body1 (subst_var ~var:var2 ~var':var1 body2)
  | ( Let { binder = var1, annot1; val_ = val1; in_ = in1 }
    , Let { binder = var2, annot2; val_ = val2; in_ = in2 } ) ->
    alpha_equal annot1 annot2
    && alpha_equal val1 val2
    && alpha_equal in1 (subst_var ~var:var2 ~var':var1 in2)
  | Fst t1, Fst t2 | Snd t1, Snd t2 -> alpha_equal t1 t2
  | ( Match
        { matchee = matchee1
        ; inl = inl_var1, inl_body1
        ; inr = inr_var1, inr_body1
        ; type_ = type1
        }
    , Match
        { matchee = matchee2
        ; inl = inl_var2, inl_body2
        ; inr = inr_var2, inr_body2
        ; type_ = type2
        } ) ->
    alpha_equal matchee1 matchee2
    && alpha_equal inl_body1 (subst_var ~var:inl_var2 ~var':inl_var1 inl_body2)
    && alpha_equal inr_body1 (subst_var ~var:inr_var2 ~var':inr_var1 inr_body2)
    && alpha_equal type1 type2
  | Kind kind1, Kind kind2 -> Kind.equal kind1 kind2
  | _, _ -> false
;;

let rec eval term =
  let open Term in
  match term with
  (* Non-reducable *)
  | Var _ | Kind _ -> term
  (* Introduction forms *)
  | Fun { binder = var, type_; body } ->
    Fun { binder = var, eval type_; body = eval body }
  | Pi { binder = var, kind; type_ } -> Pi { binder = var, eval kind; type_ = eval type_ }
  | Prod { left; right } -> Prod { left = eval left; right = eval right }
  | Sum { left; right } -> Sum { left = eval left; right = eval right }
  | Pair { fst; snd } -> Pair { fst = eval fst; snd = eval snd }
  | Inl { val_; type_ } -> Inl { val_ = eval val_; type_ = eval type_ }
  | Inr { val_; type_ } -> Inr { val_ = eval val_; type_ = eval type_ }
  (* Elimination forms *)
  | App { fun_; arg } ->
    (match eval fun_ with
    | Fun { binder = var, _; body } -> eval (subst ~var ~term:arg body)
    | fun_ -> App { fun_; arg = eval arg })
  | Fst t ->
    (match eval t with
    | Pair { fst; _ } -> fst
    | t -> Fst t)
  | Snd t ->
    (match eval t with
    | Pair { snd; _ } -> snd
    | t -> Snd t)
  | Match { matchee; inl = varl, bodyl; inr = varr, bodyr; type_ } ->
    (match eval matchee with
    | Inl { val_ = arg; _ } -> eval (subst ~var:varl ~term:arg bodyl)
    | Inr { val_ = arg; _ } -> eval (subst ~var:varr ~term:arg bodyr)
    | matchee ->
      Match
        { matchee; inl = varl, eval bodyl; inr = varr, eval bodyr; type_ = eval type_ })
  (* Let form *)
  | Let { binder; val_; in_ } ->
    eval (App { fun_ = Fun { binder; body = in_ }; arg = val_ })
;;

let beta_equal t1 t2 = alpha_equal (eval t1) (eval t2)
