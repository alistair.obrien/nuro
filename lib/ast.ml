open! Core

module Var = struct
  module T = struct
    type t = int [@@deriving sexp, compare, quickcheck]
  end

  include T
  include Comparable.Make (T)

  let create =
    let next = ref 0 in
    fun () ->
      Int.incr next;
      !next
  ;;
end

module Kind = struct
  module T = struct
    type t =
      | Box
      | Star
    [@@deriving sexp, compare, quickcheck]
  end

  include T
  include Comparable.Make (T)

  let box = Box
  let star = Star
end

module Term = struct
  type t =
    | Var of Var.t
    | App of
        { fun_ : t
        ; arg : t
        }
    | Fun of
        { binder : binder
        ; body : t
        }
    | Pi of
        { binder : binder
        ; type_ : t
        }
    | Let of
        { binder : binder
        ; val_ : t
        ; in_ : t
        }
    | Prod of
        { left : t
        ; right : t
        }
    | Pair of
        { fst : t
        ; snd : t
        }
    | Fst of t
    | Snd of t
    | Sum of
        { left : t
        ; right : t
        }
    | Inl of
        { val_ : t
        ; type_ : t
        }
    | Inr of
        { val_ : t
        ; type_ : t
        }
    | Match of
        { matchee : t
        ; inl : case
        ; inr : case
        ; type_ : t
        }
    | Kind of Kind.t
  [@@deriving sexp, quickcheck]

  and binder = Var.t * t
  and case = Var.t * t

  let rec free_vars t =
    let module Set = Var.Set in
    match t with
    | Var var -> Set.singleton var
    | Fun { binder = var, annot; body } | Pi { binder = var, annot; type_ = body } ->
      Set.(union (free_vars annot) (remove (free_vars body) var))
    | Let { binder = var, annot; val_; in_ } ->
      Set.(union_list [ free_vars annot; free_vars val_; remove (free_vars in_) var ])
    | App { fun_ = t1; arg = t2 }
    | Sum { left = t1; right = t2 }
    | Prod { left = t1; right = t2 }
    | Pair { fst = t1; snd = t2 } -> Set.union (free_vars t1) (free_vars t2)
    | Fst t | Snd t -> free_vars t
    | Inl { val_; type_ } | Inr { val_; type_ } ->
      Set.union (free_vars val_) (free_vars type_)
    | Match { matchee; inl = varl, tl; inr = varr, tr; type_ } ->
      Set.(
        union_list
          [ free_vars matchee
          ; free_vars type_
          ; remove (free_vars tl) varl
          ; remove (free_vars tr) varr
          ])
    | Kind _ -> Set.empty
  ;;

  let rec subst t ~var ~term =
    let free_vars = free_vars term in
    let rec subst_abstraction ~constr ~binder:(var', annot) body =
      if Var.equal var var'
      then constr (var', subst_term annot) body
      else if Set.mem free_vars var'
      then (
        let var'' = Var.create () in
        let body' = subst_var ~var:var' ~var':var'' body in
        constr (var'', subst_term annot) (subst_term body'))
      else constr (var', subst_term annot) (subst_term body)
    and subst_case (var', body) =
      if Var.equal var var'
      then var', body
      else if Set.mem free_vars var'
      then (
        let var'' = Var.create () in
        let body' = subst_var ~var:var' ~var':var'' body in
        var'', subst_term body')
      else var', subst_term body
    and subst_term t : t =
      match t with
      | Var var' when Var.equal var var' -> term
      | App { fun_; arg } -> App { fun_ = subst_term fun_; arg = subst_term arg }
      | Fun { binder; body } ->
        subst_abstraction ~constr:(fun binder body -> Fun { binder; body }) ~binder body
      | Pi { binder; type_ } ->
        subst_abstraction ~constr:(fun binder type_ -> Pi { binder; type_ }) ~binder type_
      | Let { binder; val_; in_ } ->
        subst_abstraction
          ~constr:(fun binder in_ -> Let { binder; val_ = subst_term val_; in_ })
          ~binder
          in_
      | Pair { fst; snd } -> Pair { fst = subst_term fst; snd = subst_term snd }
      | Fst t -> Fst (subst_term t)
      | Snd t -> Snd (subst_term t)
      | Prod { left; right } -> Prod { left = subst_term left; right = subst_term right }
      | Sum { left; right } -> Sum { left = subst_term left; right = subst_term right }
      | Inl { val_; type_ } -> Inl { val_ = subst_term val_; type_ = subst_term type_ }
      | Inr { val_; type_ } -> Inr { val_ = subst_term val_; type_ = subst_term type_ }
      | Match { matchee; inl; inr; type_ } ->
        Match
          { matchee = subst_term matchee
          ; inl = subst_case inl
          ; inr = subst_case inr
          ; type_ = subst_term type_
          }
      | Kind _ | Var _ -> t
    in
    subst_term t

  and subst_var t ~var ~var' = subst t ~var ~term:(Var var')

  module Syntax = struct
    let app (fun_ : t) (args : t list) =
      List.fold_left args ~init:fun_ ~f:(fun fun_ arg -> App { fun_; arg })
    ;;

    let fun_ type_ body =
      let x = Var.create () in
      Fun { binder = x, type_; body = body (Var x) }
    ;;

    let ( @-> ) kind type_ =
      let a = Var.create () in
      Pi { binder = a, kind; type_ = type_ (Var a) }
    ;;

    let ( @=> ) t1 t2 = t1 @-> fun _ -> t2
    let forall = ( @-> ) (Kind Star)
    let ( @: ) t1 t2 = t1, t2

    let let_ (val_, type_) ~in_ =
      let x = Var.create () in
      Let { binder = x, type_; val_; in_ = in_ (Var x) }
    ;;

    let ( * ) left right = Prod { left; right }
    let pair fst snd = Pair { fst; snd }
    let fst t = Fst t
    let snd t = Snd t
    let ( + ) left right = Sum { left; right }
    let inl val_ ~type_ = Inl { val_; type_ }
    let inr val_ ~type_ = Inr { val_; type_ }
    let match_ matchee ~with_:(inl, inr) ~type_ = Match { matchee; inl; inr; type_ }

    let case body =
      let x = Var.create () in
      x, body (Var x)
    ;;

    let kind kind = Kind kind
    let star = kind Star
    let box = kind Box
  end
end
